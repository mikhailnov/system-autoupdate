#!/bin/sh
apt-get -f -y install
apt-get update
sync
apt-get dist-upgrade -y
sync
restart_services
#apt autoremove -y

# if this variable is not set in the config, make it equivalent to 1 (true)
if [ "${ALT_update_kernel}" = '1' ] || [ -z "${ALT_update_kernel}" ]; then
	update-kernel -f
fi

sync
