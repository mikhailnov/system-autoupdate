#!/bin/sh
# This script must be compatible with POSIX shell to run on all systems
# Use shellcheck to check for POSIX compatibility

# Author: mikhailnov
# License: GPLv3

# fixes issue #3: https://gitlab.com/mikhailnov/system-autoupdate/issues/3
export DEBIAN_FRONTEND=noninteractive

dpkg --configure -a
apt-get -f -y install
apt-get update
sync
apt-get dist-upgrade -y
sync
restart_services
apt-get autoremove -y
sync
