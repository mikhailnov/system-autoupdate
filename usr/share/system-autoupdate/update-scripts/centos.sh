#!/bin/sh
restart_services
# run minimal update first, because there may be not enough free RAM for full upgrade
yum update-minimal -y
yum update -y
restart_services
# we restart services twice
# first to free some memoryy for yum, which is very hugry for it
# second to restart services which might have been updated
