#!/bin/sh
# License: GPLv3
# Author: mikhailnov
# This file contains shell functions which are used in /usr/sbin/system-autoupdate and /usr/sbin/system-autoupdate-runner

block_file="/var/tmp/system-autoupdate-reboot-guard-enabled"
# these variables can be loaded from the config file if it exists
notify_on_update="0"
notify_on_shutdown_error="1"
backup_dir="/var/backups/"

echo_help(){
	echo "
Error: Unknown arguement.
Docs: https://gitlab.com/mikhailnov/system-autoupdate
	"
}

load_config(){
	conf_file_main="/etc/system-autoupdate/system-autoupdate.conf"
	if [ -f "$conf_file_main" ]; then
		. "$conf_file_main"
	fi
}

check_root(){
	if [ ! "$(id -u)" = '0' ]
		then
			echo "Error: not root! Run as root."
			IS_ROOT='0'
		else
			IS_ROOT='1'
	fi
	
	if [ "$1" = 'exit' ] && [ ! "$IS_ROOT" = '1' ]; then
	# exit will stop the whole script
		exit 1
	fi
	
	case "$IS_ROOT" in
		'1' ) return 0 ;;
		'0' ) return 1 ;;
	esac
}

check_live(){
	# check if we are running on a LiveCD
	if mount | grep " / " | grep -qiE ' aufs | tmpfs | boot=casper ' \
		|| mount | grep " on / " | grep -qE ' type overlay| type loop'
		then
			echo "Running on a Live CD"
			LIVECD="1"
		else
			echo "Not running on a Live CD"
			LIVECD="0"
	fi
}

detect_distro(){
	# guess Linux distribution which we are running on

	if cat /etc/*release | grep -iEq 'ubuntu|mint|debian|astra'
		then platform="debian-ubuntu"
	elif cat /etc/*release | grep -Eq 'ALT|Simply Linux|altlinux'
		then platform="alt"
	elif cat /etc/*release | grep -iEq 'centos|rhel'
		then platform="centos"
	elif cat /etc/*release | grep -iq 'fedora'
		then platform="fedora"
	elif cat /etc/*release | grep -iEq 'arch|manjaro|antergos'
		then platform="arch"
	elif cat /etc/*release | grep -iq 'rosa'
		then platform="rosa"
	elif cat /etc/*release | grep -iq 'suse'
		then platform="suse"
	fi

	# override the distribution name if it's set as an environmental variable (example: env DISTRO=debian-ubuntu system-autoupdate)
	if [ "$DISTRO" ]; then platform="$DISTRO"; fi
	if [ ! "$platform" ]
		then
			echo "Unknown Linux distribution; cannot continue"
			exit 1
	fi
}
run_hooks(){
	if [ ! "$1" ]
		then
			echo "It's not specified which hooks must be ran, error."
		else
			hook="$1"
			for i in $(/bin/ls -1v /etc/system-autoupdate/hooks/${hook}/*.sh)
			do
				. "$i" "$platform"
			done
	fi
}
restart_services(){
	if [ -f "/etc/system-autoupdate/restart-services/${platform}.list" ]
		then
			restart_services_list="$(cat "/etc/system-autoupdate/restart-services/${platform}.list" | grep -v '^#' | tr '\n' ' ')"
		elif [ -f "/usr/share/system-autoupdate/restart-services/${platform}.list" ]; then
			restart_services_list="$(cat "/usr/share/system-autoupdate/restart-services/${platform}.list" | grep -v '^#' | tr '\n' ' ')"
	fi
	
	if [ -f "/etc/system-autoupdate/restart-services/general.list" ]; then
		restart_services_list="${restart_services_list} $(cat "/etc/system-autoupdate/restart-services/general.list" | grep -v '^#' | tr '\n' ' ')"
	fi
	
	if [ ! -z "$restart_services_list" ]; then 
		systemctl restart ${restart_services_list}
	fi
}
zram_restart(){
	if [ "$zram_was_active" = '1' ]
		then
			systemctl restart zram-config || systemctl restart zram-start || systemctl restart zramswap || systemctl restart zram
	fi
}
update(){
	# run pre-update hooks (scripts)
	run_hooks pre-update

	#############
	# run platform specific system update script
	. "/usr/share/system-autoupdate/update-scripts/${platform}.sh"
	
	# update snap packages if snap is installed
	if  [ "$snap_refresh" = '1' ] && snap --version >/dev/null; then
		snap refresh
	fi
	
	#############
	# run post-update hooks (scripts)
	run_hooks post-update

	sync
	
	if [ "$swap_restart" = '1' ]; then
		if [ -f /dev/zram0 ]
			then
				zram_was_active="1"
		fi
		swapoff -a && swapon -a && zram_restart
		sync
	fi
}

block_shutdown() {
	systemctl start system-autoupdate-reboot-guard system-autoupdate-start-reboot-guard
	touch "$block_file"
}

unblock_shutdown() {
	systemctl stop system-autoupdate-reboot-guard system-autoupdate-start-reboot-guard
	systemctl disable system-autoupdate-reboot-guard system-autoupdate-start-reboot-guard
	rm -fv "$block_file"
}

run_update(){
# run system-autoupdate without systemd-inhibit if systemd-inhibit is not availbale
	systemd-inhibit \
		--who="system-autoupdate" \
		--why="Updating system! Please shutdown after updates are installed! Происходит установка обновлений, пожалуйста, выключите компьютер позже, когда обновления будут установлены!" \
		/usr/sbin/system-autoupdate \
	|| /usr/sbin/system-autoupdate
}

notify_all() {
# based on https://unix.stackexchange.com/a/344377
	urgency="$1"
	icon="$2"
	title="$3"
	msg="$4"
	if [ -x "$(which notify-send)" ]; then
		who | grep tty | awk '{print $1, $NF}' | tr -d "()" |
		while read -r u d; do
			##id="$(id -u "$u")"
			DBUS_SESSION_BUS_ADDRESS_tmp="$(ps -u "$u" e | tr ' ' '\n' | grep DBUS_SESSION_BUS_ADDRESS | sort -u | tail -n 1)"
			DBUS_SESSION_BUS_ADDRESS="$(echo "$DBUS_SESSION_BUS_ADDRESS_tmp" | sed 's/^DBUS_SESSION_BUS_ADDRESS=//g' | awk -F ',' '{print $1}')"
			# why ' inside " ? try: echo "'$TERM'" and echo '$TERM'
			# '|| true' to workaround 'polkit spawing helper error' which may block session startup on KDE
			su "$u" -c "env DISPLAY="$d" DBUS_SESSION_BUS_ADDRESS="${DBUS_SESSION_BUS_ADDRESS}" notify-send --urgency='$urgency' --icon='$icon' '$title' '$msg' || true" || true
		done 
	fi
}

notify_all_if_config(){
	if [ "$notify_on_update" = '1' ]; then
		notify_all "$@"
	fi
}

get_unit_status() {
	# it's possible to do `systemctl show -p SubState --value "$1"` without sed
	# but it's available in systemd>=230, and Ubuntu 16.04 has systemd 229
	# https://unix.stackexchange.com/a/435317
	unit_status="$(systemctl show -p SubState "$1" | sed 's/SubState=//g')"

	case "$unit_status" in
	"failed" )
		return 1
	;;
	* )
		return 0
	;;
	esac
}

check_shutdown() {
# this function is called from Polkit rule
	
	# system-autoupdate.service may fail for some reason, e.g. https://gitlab.com/mikhailnov/system-autoupdate/issues/3
	# so we must allow shutting down the system if system-autoupdate.service failed (was killed before end of work)
	# and did not remove $block_file
	if ! get_unit_status system-autoupdate.service; then
		# don't try to unlock_shutdown if we are not ran as root:
		# external applications or users may run check_shutdown as not root
		# and all we have to do is just returning 1 or 0
		# otherwise systemctl will prompt for password
		check_root && unblock_shutdown
	fi
	if [ -f "$block_file" ]
		then
			# disallow shutdown if the blocking file exists
			return 1
		else
			return 0
	fi  
}

service_status(){
	backup_file="${backup_dir}/system-autoupdate-control-sudo.bak"
	
	case "$1" in
		'enable')
			check_root exit
			detect_distro
			
			# control sudo public is for running sudo from user polkitd
			# why we need to run sudo from polkit — see https://gitlab.com/mikhailnov/system-autoupdate/issues/2
			if [ "$platform" = 'alt' ] && [ -x "$(which control)" ]; then
				control_sudo_state1="$(control sudo)"
				if [ ! -z "$control_sudo_state1" ]; then
					if [ ! -d "${backup_dir}" ]; then mkdir -p "${backup_dir}"; fi
					echo "$control_sudo_state1" > "${backup_file}"
				fi
				sh -x -c "control sudo public"
			fi
			
			systemctl daemon-reload
			systemctl enable system-autoupdate.timer system-autoupdate-sanity.service
		;;
		'disable')
			check_root exit
			detect_distro
			
			if [ -f "${backup_file}" ] && [ -x "$(which control)" ]; then
				control_sudo_state_bak="$(cat "${backup_file}")"
				if [ ! -z "$control_sudo_state_bak" ]; then
					control sudo "$control_sudo_state_bak" && \
					echo "control sudo reverted to ${control_sudo_state_bak}, see altlinux.org/Control for more information"
				fi
			fi
			
			unblock_shutdown
			systemctl disable system-autoupdate.timer system-autoupdate-sanity.service
		;;
		'status')
			systemctl status system-autoupdate.timer system-autoupdate-sanity.service
		;;
	esac
}

log(){
	journalctl -xb | grep system-autoupdate
}

#########

load_config
