system-autoupdate (1.16-1) unstable; urgency=medium

  * Improved detecting LiveCD (fixed detecting Ubuntu LiveCD)

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Thu, 02 May 2019 21:00:00 +0300

system-autoupdate (1.15-2) unstable; urgency=medium

  * disable swapoff by default
    (it's not a good idea to try to turn swap off in a random moment of time)
  
 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Fri, 01 Feb 2019 00:43:00 +0300
 
system-autoupdate (1.14-1) unstable; urgency=medium

  * Version 1.14
  * Fixed typo: did not unblock shutdown when systemd unit was in failed state
  * Allow custom systemd unit directory in Makefile
  
 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Mon, 12 Nov 2018 03:05:00 +0300

system-autoupdate (1.13-2) unstable; urgency=medium

  * Version 1.13
  * commands enable, disable, status, check_root
  * symlink /usr/bin/saur --> /usr/sbin/system-autoupdate
  
 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Wed, 12 Sep 2018 02:46:00 +0300

system-autoupdate (1.12-1) unstable; urgency=medium

  * Version 1.12
  * Improved systemd presets
  * Fixed sending notifications on ALT Linux

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Thu, 30 Aug 2018 13:58:00 +0300

system-autoupdate (1.11-1) unstable; urgency=medium

  * Version 1.11
  * change 'apt-get -f install' to 'apt-get -f -y install'

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Tue, 31 Jul 2018 13:26:00 +0300
 
system-autoupdate (1.10-1) unstable; urgency=low

  * Version 1.10
  * Do not run update when running on Live CD

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Sat, 28 Jul 2018 12:17:00 +0300

system-autoupdate (1.9-1) unstable; urgency=low

  * Version 1.9
  * Fixed https://gitlab.com/mikhailnov/system-autoupdate/issues/3

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Sat, 21 Jul 2018 20:13:00 +0300
 
system-autoupdate (1.8-2) unstable; urgency=low

  * Version 1.8
  * New config options: snap_refresh, swap_restart
  * Fixed workaround of https://gitlab.com/mikhailnov/system-autoupdate/issues/3: now check_shutdown is ran as root

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Fri, 06 Jul 2018 01:45:00 +0300 
 
system-autoupdate (1.7-2) unstable; urgency=low

  * Version 1.7
  * Refactoring: move all functions to /usr/share/system-autoupdate/common-funcs.sh and source them from /usr/sbin/system-autoupdate and /usr/sbin/system-autoupdate-runner
  * Increase system-autoupdate.timer delay after startup to 5 min to workaround https://gitlab.com/mikhailnov/system-autoupdate/issues/2
  * Allow shutdown if system-autoupdate.service is in 'failed' condition (https://gitlab.com/mikhailnov/system-autoupdate/issues/3)
  * Add debian/format/source: '3.0 (git)'
  * ALT Linux: add option to turn on/off running 'update-kernel -f'

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Mon, 02 Jul 2018 15:56:00 +0300 
 
system-autoupdate (1.6-1) unstable; urgency=low

  * Version 1.6
  * ConditionACPower=true for system-autoupdate.service

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Sun, 25 Jun 2018 17:20:00 +0300 
 
system-autoupdate (1.5-1) unstable; urgency=low

  * Version 1.5
  * Conflict with unattended-upgrades
  * Pevent running system-autoupdate.service with potentially conflictable systemd units at the same time: apt-daily.service, apt-daily-upgrade.service, dnf-makecache.service

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Sun, 24 Jun 2018 20:15:00 +0300 
 
system-autoupdate (1.4-1) unstable; urgency=low

  * Version 1.4
  * Small fixes & improvements
  * Some changes in systemd services and timers

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Sat, 16 Jun 2018 17:55:00 +0300 
 
system-autoupdate (1.3-3) unstable; urgency=low

  * Version 1.3

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Wed, 13 Jun 2018 15:05:00 +0300 

system-autoupdate (1.2-4) unstable; urgency=low

  * Build of version 1.2

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Wed, 13 Jun 2018 00:29:00 +0300 

system-autoupdate (1.1-1) unstable; urgency=low

  * Fix typo (bug)

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Sat, 09 Jun 2018 21:23:00 +0300 
 
system-autoupdate (1.0-3) unstable; urgency=low

  * New version 1.0 (first stable version)

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Sat, 09 Jun 2018 19:45:00 +0300 
 
system-autoupdate (0.4-4) unstable; urgency=low

  * New version 0.4

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Fri, 08 Jun 2018 22:32:00 +0300 

system-autoupdate (0.3-3) unstable; urgency=low

  * First build

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Thu, 07 Jun 2018 20:59:00 +0300 
